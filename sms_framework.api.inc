<?php
/**
 * Implementation of hook_ctools_plugin_directory().
 */
function HOOK_ctools_plugin_directory($module, $plugin) {
  if ($module == 'your_module_name' && $plugin == 'sms_vendor') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Then create dir plugins/sms_vendor in your module path.
 * the specific code, pls reference:sms_framework/plugins/sms_vendor/
 */
