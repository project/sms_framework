<?php
/**
 * Implementation of hook_views_default_views().
 */
function sms_framework_views_default_views() {
  $view = new view();
  $view->name = 'sms_message';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sms_message';
  $view->human_name = 'sms message';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'sms message';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'admin sms message';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: SMS Message: Unique ID */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'sms_message';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['label'] = 'ID';
  $handler->display->display_options['fields']['sid']['separator'] = '';
  /* Field: SMS Message: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'sms_message';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: SMS Message: Mobile */
  $handler->display->display_options['fields']['mobile']['id'] = 'mobile';
  $handler->display->display_options['fields']['mobile']['table'] = 'sms_message';
  $handler->display->display_options['fields']['mobile']['field'] = 'mobile';
  $handler->display->display_options['fields']['mobile']['label'] = 'Label';
  /* Field: SMS Message: created Time */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'sms_message';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: SMS Message: Expire Time */
  $handler->display->display_options['fields']['expired']['id'] = 'expired';
  $handler->display->display_options['fields']['expired']['table'] = 'sms_message';
  $handler->display->display_options['fields']['expired']['field'] = 'expired';
  $handler->display->display_options['fields']['expired']['label'] = 'Expired';
  $handler->display->display_options['fields']['expired']['date_format'] = 'long';
  $handler->display->display_options['fields']['expired']['second_date_format'] = 'long';
  /* Field: SMS Message: SMS Content */
  $handler->display->display_options['fields']['sms_body']['id'] = 'sms_body';
  $handler->display->display_options['fields']['sms_body']['table'] = 'sms_message';
  $handler->display->display_options['fields']['sms_body']['field'] = 'sms_body';
  $handler->display->display_options['fields']['sms_body']['label'] = 'Sms_body';
  /* Field: SMS Message: IP */
  $handler->display->display_options['fields']['ip']['id'] = 'ip';
  $handler->display->display_options['fields']['ip']['table'] = 'sms_message';
  $handler->display->display_options['fields']['ip']['field'] = 'ip';
  /* Sort criterion: SMS Message: created Time */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'sms_message';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/structure/sms-message';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'SMS Message';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['sms_message'] = array(
    t('Master'),
    t('sms message'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('ID'),
    t('.'),
    t('Type'),
    t('Label'),
    t('Created'),
    t('Expired'),
    t('Sms_body'),
    t('IP'),
    t('Page'),
  );

  $views[$view->name] = $view;

  return $views;
}

