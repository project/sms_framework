<?php
/**
 * Entity Controller.
 */
class EntitySmsType extends Entity {
  public function setUp() {
    parent::setUp();
    $this->updated = REQUEST_TIME;
    if (!isset($this->created) || empty($this->created)) {
      $this->created = REQUEST_TIME;
    }
    if (!isset($this->module) || empty($this->module)) {
      $this->module = 'sms_framework';
    }
  }
  public function save() {
    parent::save();
  }
}

/**
 * UI controller
 */
class SmsTypeUIController extends EntityDefaultUIController {
}
