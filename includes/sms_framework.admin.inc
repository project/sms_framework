<?php
/**
 * @file
 * Contains pages for creating, editing, and deleting SMS Type.
 */

/**
 * Generate the SMS Type editing form.
 */
function sms_type_form($form, $form_state, $sms_type, $op = 'edit') {
  // Clone.
  if ($op == 'clone') {
    $sms_type->name .= '';
    $sms_type->label .= ' (cloned)';
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => isset($sms_type->is_new) ? NULL : $sms_type->label,
    '#description' => t('A short, descriptive label for this SMS Type.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#default_value' => isset($sms_type->is_new) ? NULL : $sms_type->name,
    '#description' => t('The machine-name for this SMS type.'),
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => 'sms_type_load',
      'source' => array('label'),
    ),
    '#required' => TRUE,
    '#disabled' => !(isset($sms_type->is_new) && $sms_type->is_new),
  );
  $form['vendor'] = array(
    '#type' => 'select',
    '#options' => sms_framework_get_vendor_list_enabled(),
    '#default_value' => isset($sms_type->is_new) ? 'sms_framework' : $sms_type->vendor,
    '#description' => t('短信提供商'),
  );
  $form['expire_time'] = array(
    '#type' => 'textfield',
    '#title' => t('过期时间'),
    '#description' => t('单位：秒'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => isset($sms_type->is_new) ? 60 : $sms_type->expire_time,
    '#required' => TRUE,
  );
  $form['tpl'] = array(
    '#type' => 'textarea',
    '#title' => t('短信模版'),
    '#default_value' => isset($sms_type->is_new) ? NULL : $sms_type->tpl,
    '#description' => t('Please start with 【prefix】'),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/structure/sms-type',
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function sms_type_form_submit($form, &$form_state) {
  $sms_type = entity_ui_form_submit_build_entity($form, $form_state);
  $sms_type->save();

  $form_state['redirect'] = 'admin/structure/sms-type';
}
