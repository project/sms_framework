<?php
/**
 * send verify code.
 */
function _sms_framework_verify_code_send($mobile, $type) {
  $code = rand(100000, 999999);
  if ($res = sms_message_send($mobile, $type, array('[code]' => $code))) {
    return $res;
  }
  else {
    return services_error(t('Send message failed, please contact site admin for more information'), 401);
  }
}

/**
 * validate mobile
 */
function _sms_framework_validate_mobile($mobile) {
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'user')
    ->fieldCondition('field_mobile_phone', 'value', $mobile);
  $res = $efq->execute();
  if (!empty($res['user'])) {
    return array('status' => TRUE);
  }
  else {
    return array('status' => FALSE);
  }
}

/**
 * Validate code
 */
function _sms_framework_verify_code_validate($mobile, $code, $type = 'reset_password') {
  $sms_types = sms_type_load();
  foreach ($sms_types as $sms_type) {
    $sms_content[] = str_replace('[code]', $code, $sms_type->tpl);
  }
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'sms_message')
    ->propertyCondition('mobile', $mobile)
    ->propertyCondition('sms_body', $sms_content, 'IN')
    ->propertyCondition('expired', REQUEST_TIME, '>=');
  $res = $efq->execute();
  if (empty($res['sms_message'])) {
    return array(
      'status' => FALSE,
    );
  }
  else {
    return array(
      'status' => TRUE,
    );
  }
}
