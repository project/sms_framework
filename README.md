#overview/概览
* 该模块基于 ctools, views, entity开发. 扩展性极佳。

#support sms vendor/支持的短信提供商
* Emay/亿美
* Tricom/大汉三通
* ZKF/我也不知道中文叫什么，发送到短信接口地址是：http://t3.netsms.cn

#usage/模块使用
- goto admin/structure/sms-type/settings

- fill in the credentials which are different for each sms vendor./填写相关的认证资料，不同短信供应商所需填写的认证资料有所不同。

- 如果没有短信供应商，认证资料可以随便填写0已通过认证。这里注意，启用需要的短信供应商。

- goto admin/structure/sms-type, click Add sms type

- 填写短信配置的相关信息。注意的是，短信模板哪里，需要根据实际情况决定是否要添加 "【公司抬头】"，因为有的短信提供商自带这个，有的需要加这个才给你发短信。请根据实际情况灵活把握。

- 在你程序需要的地方，调用sms_message_send($mobile,$type)即可发送短信。$type是上一步创建的短信类型的机器名。
- 短信的发送记录，可以在admin/structure/sms-message看到。这是一个views页面。可以根据实际情况灵活配置。

#使用自定义的短信供应商
- 参考sms_framework.api.inc
