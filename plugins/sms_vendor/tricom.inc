<?php
/**
 * @file
 * Sms vendor plugin:Emay sms sendor.
 */
$plugin = array(
  'label' => t('大汉三通'),
  'description' => t("大汉三通"),
  'handler' => array(
    'class' => 'TricomSmsVendor',
  ),
);

/**
 * Sms Vendor class.
 */
class TricomSmsVendor implements SmsVendor {
  /**
   * {@inheridoc}.
   */
  public function settingsForm($form = NULL, $form_state = NULL) {
    $form['tricom_username'] = array(
      '#type' => 'textfield',
      '#title' => t('用户名'),
      '#required' => TRUE,
      '#default_value' => variable_get('tricom_username', ''),
    );
    $form['tricom_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#default_value' => variable_get('tricom_password', ''),
    );
    return $form;
  }
  /**
   * {@inheridoc}.
   *@param $isvoice 是否语音短信,若为空默认为普通短信.该参数格式:
   *是否语音(0表示普通短信,1表示语音短信,2表示信令短信)|重听次数|重拨次数|是否回复(0表示不回复,1表示回复)
   *如:isvoice=”1|1|2|0” 即:语音短信,重听次数1,重拨次数2,不回复
   */
  public function send($mobile, $content, $isvoice='') {
    $cdkey = variable_get('tricom_username', '');
    $password = variable_get('tricom_password', '');
    $url = 'http://gateway.iems.net.cn/GsmsHttp';
    $isvoice = !empty($isvoice) ? '1|3|2|0' : '';
    $query = array(
      'username' => $cdkey,
      'password' => $password,
      'from' => '001',
      'to' => $mobile,
      'content' => iconv('UTF-8', 'GBK', $content),
      'expandPrefix' => '927927',
      'isvoice' => $isvoice,
    );
    $request_url = url($url, array('query' => $query));
    $resp = drupal_http_request($request_url);
    if ($resp->code == 200) {
      return $resp;
    }
    else {
      watchdog('tricom error', '<pre>' . print_r($resp, TRUE) . '</pre>');
      return FALSE;
    }
  }
}
