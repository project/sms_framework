<?php
/**
 * @file
 * Sms vendor plugin:Emay sms sendor.
 */
$plugin = array(
  'label' => t('Emay'),
  'description' => t("Emay message sendor"),
  'handler' => array(
    'class' => 'EmaySmsVendor',
  ),
);

/**
 * Emay Vendor class.
 */
class EmaySmsVendor implements SmsVendor {
  /**
   * {@inheridoc}.
   */
  public function settingsForm($form = NULL, $form_state = NULL) {
    $form['emay_cdkey'] = array(
      '#type' => 'textfield',
      '#title' => t('CDKEY'),
      '#required' => TRUE,
      '#default_value' => variable_get('emay_cdkey', ''),
    );
    $form['emay_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#default_value' => variable_get('emay_password', ''),
    );
    return $form;
  }
  /**
   * {@inheridoc}.
   */
  public function send($mobile, $content) {
    $cdkey = variable_get('emay_cdkey', '');
    $password = variable_get('emay_password', '');
    $url = 'http://hprpt2.eucp.b2m.cn:8080/sdkproxy/sendsms.action';
    $query = array(
      'cdkey' => $cdkey,
      'password' => $password,
      'phone' => $mobile,
      'message' => $content,
    );
    $request_url = url($url, array('query' => $query));
    $resp = drupal_http_request($request_url);
    watchdog('emay response', '<pre>' . print_r($resp, TRUE) . '</pre>');
    if ($resp->code == 200) {
      return $resp;
    }
    else {
      watchdog('emay_sms error', '<pre>' . print_r($resp, TRUE) . '</pre>');
      return FALSE;
    }
  }
}
