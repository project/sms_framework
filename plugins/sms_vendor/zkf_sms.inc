<?php
/**
 * @file
 * Sms vendor plugin:ZKF sms sendor.
 */
$plugin = array(
  'label' => t('ZKF'),
  'description' => t("ZKF message sendor"),
  'handler' => array(
    'class' => 'ZkfSmsVendor',
  ),
);

/**
 * ZKF sms Vendor class.
 */
class ZkfSmsVendor implements SmsVendor {
  /**
   * {@inheridoc}.
   */
  public function settingsForm($form = NULL, $form_state = NULL) {
    $form['zkf_sms_user_id'] = array(
      '#type' => 'textfield',
      '#title' => t('UserId'),
      '#default_value' => variable_get('zkf_sms_user_id', ''),
      '#required' => TRUE,
    );
    $form['zkf_sms_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => variable_get('zkf_sms_password', ''),
      '#required' => TRUE,
    );
    return $form;
  }
  /**
   * {@inheridoc}.
   */
  public function send($mobile, $content) {
    $url = 'http://t3.netsms.cn:9013/MWGate/wmgw.asmx';
    $data = zkf_message_build_request_xml($mobile, $content);
    $options = array(
      'method' => 'POST',
      'data' => $data,
    );
    $resp = drupal_http_request($url, $options);
    watchdog('zkf_sms response', '<pre>' . print_r($resp, TRUE) . '</pre>');
    if ($resp->code == 200) {
      return $resp;
      $doc = new DOMDocument('1.0', 'utf-8');
      $doc->loadXML($req->data);
      $XMLresults = $doc->getElementsByTagName("MongateCsSpSendSmsNewResult");
      $result = $XMLresults->item(0)->nodeValue;
      return $result;
    }
    else {
      watchdog('zkf_sms error', '<pre>' . print_r($resp, TRUE) . '</pre>');
      return FALSE;
    }
  }
}

/**
 * helper function to wrapper the request XML.
 */
function zkf_message_build_request_xml($mobile, $content) {
  $user_id = variable_get('zkf_sms_userid', '');
  $password = variable_get('zkf_sms_password', '');
  $xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <MongateCsSpSendSmsNew xmlns="http://tempuri.org/">
      <userId>{$user_id}</userId>
      <password>{$password}</password>
      <pszMobis>{$mobile}</pszMobis>
      <pszMsg>{$content}</pszMsg>
      <iMobiCount>1</iMobiCount>
      <pszSubPort>*</pszSubPort>
  <SvrType>string</SvrType >
  <Param1>p1</Param1>
  <Param2>p2</Param2>
  <Param3>p3</Param3>
  <Param4>p4</Param4>
  <MsgId>MsgId</MsgId>
  <ModuleId>ModuleId</ModuleId>
      <Attime></Attime>
  <Validtime></Validtime>
  <RptFlag>0</RptFlag>
    </MongateCsSpSendSmsNew>
  </soap:Body>
</soap:Envelope>
XML;
  return $xml;
}
