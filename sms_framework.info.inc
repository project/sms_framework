<?php

/**
 * @file
 * Provides metadata for the entities.
 */

/**
 * Implements hook_entity_property_info().
 */
function sms_framework_entity_property_info() {
  $info = array();
  // Add meta-data about the sms_type properties.
  $properties = &$info['sms_type']['properties'];
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the sms_type was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  $properties['updated'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the sms_type was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'updated',
  );
  $properties['label'] = array(
    'label' => t('Label'),
    'description' => t('The human-readable name of the sms_type template.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'label',
  );
  $properties['name'] = array(
    'label' => t('Name'),
    'description' => t('The unified identifier for the sms_type template.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'name',
  );

  // Add meta-data about the basic entity_email properties.
  $properties = &$info['sms_message']['properties'];
  $properties['sid'] = array(
    'label' => t("Unique ID"),
    'type' => 'integer',
    'description' => t("The Drupal unique ID."),
    'schema field' => 'sid',
  );
  $properties['type'] = array(
    'label' => t("Type"),
    'type' => 'token',
    'description' => t("The type of this sms."),
    'schema field' => 'type',
    'options list' => 'sms_framework_get_vendor_list',
  );
  $properties['mobile'] = array(
    'label' => t("Mobile"),
    'type' => 'text',
    'description' => t("The Mobile Number."),
    'schema field' => 'mobile',
  );
  $properties['ip'] = array(
    'label' => t("IP"),
    'type' => 'text',
    'description' => t("The ip address."),
    'schema field' => 'ip',
  );
  $properties['sms_body'] = array(
    'label' => t("SMS Content"),
    'type' => 'text',
    'description' => t("When the message body Content."),
    'schema field' => 'sms_body',
  );
  $properties['expired'] = array(
    'label' => t("Expire Time"),
    'type' => 'date',
    'description' => t("The expire time of the sms"),
    'schema field' => 'expired',
  );
  $properties['created'] = array(
    'label' => t("created Time"),
    'type' => 'date',
    'description' => t("The create time of the sms"),
    'schema field' => 'created',
  );
  return $info;
}
