<?php
/**
 * sms vendor settings form.
 */
function sms_framework_settings_form() {
  ctools_include('plugins');
  $plugins = ctools_get_plugins('sms_framework', 'sms_vendor');
  foreach ($plugins as $key => $plugin) {
    $vendor = ctools_plugin_get_class($plugin, 'handler');
    $smsController = new $vendor();
    $settings_form = $smsController->settingsForm();

    $form[$key] = array(
      '#type' => 'fieldset',
      '#title' => $plugin['label'],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ) + $settings_form;

    $form[$key][$key . '_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value' => variable_get($key . '_enable', TRUE),
    );
  };
  $form['sms_framework_enable_limit'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('sms_framework_enable_limit', TRUE),
    '#title' => '是否启用发送限制? ( IP一天200条   同一用户一天5条 1小时2条)',
  );
  return system_settings_form($form);
}
